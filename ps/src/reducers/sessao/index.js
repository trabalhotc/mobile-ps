const initialState={
    aluno:{},
    turmaDisciplina :{
        id : null,
        nome :  "" ,
        idDisciplina : null,
        idTurma : null,
        turma : {
            id : null,
            nome : null 
       },
        disciplina : {
            id : null,
            nome :  null 
       }
    },
    isAuthenticated: false,
    idSessao: null,
    idTurmaDisciplina: null,
}

const SessaoReducer = (state = [],action) =>{
    if (state.length == 0 ){
        return initialState;
    }else{
        switch(action.type){
        
            case 'LOGIN_ALUNO_SESSAO' :                
                return{                      
                    ...state,
                    turmaDisciplina : action.payload.turmaDisciplina,
                    ...action.payload                    
                };
              
            default:
                return state;
        }
    }

};

export default SessaoReducer;