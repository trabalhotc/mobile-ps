const initialState={
    id:null,
    nome : null,
    cpf:null,
    senha:null,
    //turmaDisciplina :{}
}

const AlunoReducer = (state = [],action) =>{
    if (state.length == 0 ){
        return initialState;
    }else{
        switch(action.type){
            case 'INSERT_ALUNO' :                
                return{                      
                    initialState                    
                };
            case 'LOGIN_ALUNO' :                
                return{                      
                    ...state,
                    ...action.payload                    
                };
            case 'CHANGE_VALUE_ALUNO' :
                return{  
                    ...state,
                    ...action.payload                                                          
                };       
            default:
                return state;
        }
    }

};

export default AlunoReducer;