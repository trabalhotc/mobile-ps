import { combineReducers } from 'redux';

import SessaoReducer  from './sessao';
import AlunoReducer  from './aluno';

import navReducer from './navigation';

export const Reducers = combineReducers({
    alunoReducer    : AlunoReducer,
    sessaoReducer   : SessaoReducer,
    //nav             : navReducer
});
