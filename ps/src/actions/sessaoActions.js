import AlunoService from '../service/alunoService';
import {Alert} from 'react-native';
import {AsyncStorage} from 'react-native';
import { NavigationActions } from 'react-navigation';
import NavigationService from '../NavigationService';
import {recreateConnections} from '../service/api';


export const updateConnectionServer = () =>{
    
    return (dispatch) =>{
        recreateConnections();
        
    } 
};

export const loginAuluno = (aluno) =>{
    return (dispatch) =>{
        try{            
            
            AlunoService.login(aluno)
            .then(  response  => {    
                if (response.data.isAuthenticated){
                     AsyncStorage.setItem('@PS-ALUNOLOGADO', 'sim'); 
                     AsyncStorage.setItem('@PS-IDSESSAO',response.data.idSessao);
                     AsyncStorage.setItem('@PS-NOMETURMA',response.data.turmaDisciplina.nome );
                    //await AsyncStorage.setItem('@PS-IDSESSAO',response.data.idSessao);
                    //Alert.alert(response.data.aluno.nome); 
                    console.log(response.data);
                    dispatch({
                        
                        type: 'LOGIN_ALUNO_SESSAO',
                        payload:{
                            isAuthenticated   : response.data.aluno.isAuthenticated,
                            idSessao          : response.data.aluno.idSessao,
                            idTurmaDisciplina : response.data.aluno.idTurmaDisciplina,
                            turmaDisciplina   : response.data.turmaDisciplina,
                            aluno             : response.data.aluno
                        }
                    });
                    //cuidado com isso
                    //nav();
                    NavigationService.navigate('Home');


                }else{
                    Alert.alert('Aviso', response.data.message)
                    //throw new error.data.message
                }
                
                

            })
            .catch(error => {
                //console.log(error);
                if (error.data.isAuthenticated=false){
                    //Alert.alert('Aviso', 'CPF ou senha digitados estão incorretos ou não existem!')
                    Alert.alert('Aviso', error.data.message)
                };
                //throw error.data.message
                console.log('erro connect:'+error);
                
            })
        }catch(error){
            Alert.alert(error)
        };

    };
    
};
