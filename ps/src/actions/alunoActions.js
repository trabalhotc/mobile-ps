import AlunoService from '../service/alunoService';
import {Alert} from 'react-native';

export const list = () =>{
    return (dispatch) =>{
        AlunoService.findAll()
            .then((response) =>{
                dispatch({
                    type: 'LIST_ALUNO',
                    payload:{
                        turmas : response.data
                    }
                });
            })

    };
    
};



export const insertAluno = (aluno) =>{
    return (dispatch) =>{
        try{            
            AlunoService.save(aluno)
            .then( response => {                            
                Alert.alert('Informação','Cadastro concluído!');               
                dispatch({
                    type: 'INSERT_ALUNO',
                    payload:{
                        id    : response.data.aluno.id,
                        nome  : response.data.aluno.nome,
                        CPF   : response.data.aluno.CPF,
                        senha : response.data.aluno.senha,
                    }
                });
                

            })
            .catch(error => {
                throw error;  
            })
        }catch(error){
            Alert.alert('Erro',error)
        };

    };
    
};


export const changeValue = (name ,value) =>{
    
    return{
        type:'CHANGE_VALUE_ALUNO',
        payload:{
            [name] : value
        }
    }
    
};