import {openModalInfo} from './modalInfo'


export function modalInfo(obj) {
    openModalInfo({ message: obj.message || obj.toString()});
}