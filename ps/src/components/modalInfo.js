import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert} from 'react-native';


let openModalFn;
class ModalInfo extends Component {
  state = {
    modalVisible: false,
    message : ""
  };

  setModalVisible =(visible,message) => {
    this.setState({ modalVisible: visible, message});

  }

  componentDidMount =()=>{
    openModalFn = this.setModalVisible();
  }

  render() { 
    return (
      <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{marginTop: 22}}>
            <View>
              <Text>{this.state.message}</Text>

              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible,'');
                }}> 
                <Text>Ok</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
};

export function openModalInfo({ message }) {
    const open = false;
    openModalFn({open, message });
}

//export default withStyles(styles)(Notifier);
export default ModalInfo;