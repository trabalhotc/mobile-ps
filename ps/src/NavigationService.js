import { NavigationActions, NavigationState } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function getActiveRouteState (route) {
  if (!route.routes || route.routes.length === 0 || route.index >= route.routes.length) {
      return route;
  }

  const childActiveRoute = route.routes[route.index] ;
  return getActiveRouteState(childActiveRoute);
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  getActiveRouteState
};

