import React, { Component } from 'react';

import { 
  View , 
  Button,
  Text, 
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Animated,
  Easing,
  ActivityIndicator,
  ImageBackground} from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import {Icon} from 'react-native-elements'
import ImageLoader from '../../components/imageLoader';
import  socket from 'socket.io-client';
// import { Container } from './styles';
import Constants from '../../constants';
import NavigationService from '../../NavigationService';
import NotifService from '../../service/NotifService';
import {io} from '../../service/api';


  


const BG_IMAGE = require('../../images/bkg.png');
const LOGO_IMAGE = require('../../images/logo_simples.png');




export class Home extends Component {

  constructor(props){
    super(props);
    this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this));
  }
  state ={
    cont : 0,
    rotateAnim: new Animated.Value(0)
  }

  onRegister(token) {
    //Alert.alert("Registered !", JSON.stringify(token));
    //console.log(token);
    //this.setState({ registerToken: token.token, gcmRegistered: true });
  }

  onNotif(notif) {
    //console.log(notif);
    //Alert.alert(notif.title, notif.message);
  }

  startAnimation () {
    this.state.rotateAnim.setValue(0)
    Animated.timing(
      this.state.rotateAnim,
      {
        toValue: 1,
        duration: 12000,
        easing: Easing.linear
      }
    ).start(() => {
      this.startAnimation()
    })
  }

  static navigationOptions = ( {navigation} ) =>({
    
    title: navigation.state.params ? navigation.state.params.myTitle : 'Titulo',//navigation.state.params.myTitle ? navigation.state.params.myTitle : ''
    
    headerStyle: {
      backgroundColor: '#203349',
      
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color:'#FDFDFD'
    },
    headerRight : (
      <TouchableOpacity style={{marginRight:8}} onPress={() =>{
        AsyncStorage.setItem('@PS-ALUNOLOGADO', ""); 
        AsyncStorage.setItem('@PS-IDSESSAO',"");
        AsyncStorage.setItem('@PS-NOMETURMA',"");
        navigation.navigate('Login');
       }} >
          <Icon
            name='exit-to-app'
            color='#FDFDFD' 
            >

          </Icon>
      </TouchableOpacity>
        
     
    ),
    
  });

  
  subscribetoEvents = ()=>{
    //const io = socket(Constants.BASE_URL_SERVER);

    
    
    io()
      .then(instace =>{
        instace.on('perguntaApresentada', async data=>{
          //Alert.alert('teste');
          const logado = await  AsyncStorage.getItem('@PS-ALUNOLOGADO');

          
          if (logado=="sim"){
            this.props.navigation.navigate('Responder');
            this.notif.cancelAll();
            this.notif.localNotif();
          }
          //Alert.alert('logado = nao');
          
        });

        instace.on('pontuacaoAtualizada', async ()=>{
          const logado = await  AsyncStorage.getItem('@PS-ALUNOLOGADO');

          
          if (logado=="sim"){
            this.props.navigation.navigate('Home');
          }
          
          
        });

        
      })

    /*
    io.on('encerrouSessao', ()=>{
      console.log('encerrouSessao');
      this.props.navigation.navigate('Login');
      
    }); 
    */
    
   
  }

  componentDidMount = async ()=>{
    //Alert.alert(this.props.aluno.CPF);
    this.props.navigation.setParams({
      myTitle: await  AsyncStorage.getItem('@PS-NOMETURMA')
     
     });
     
     this.subscribetoEvents();
     this.startAnimation();
     
     

     
  };

  
  

  render() {
    return(
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          <Animated.Image
              source={LOGO_IMAGE}
              style={[
                styles.logoAnimago,
                {transform: [
                  {rotate: this.state.rotateAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                      '0deg', '360deg'
                    ]
                  })}
                ]} ]}
            />
          <View style={styles.formContainer}>
            
            <Text style={styles.textAguardando} onPress={()=>{this.notif.localNotif()}}>
              Aguardando Perguntas
              
            </Text>
            
            
          </View>
          
        </ImageBackground>
      </View>
    )
  }
};

const mapStateToProps = (state) =>{
  return{ 
      nomeAluno  : state.sessaoReducer.aluno.nome,
      aluno      : state.sessaoReducer.aluno,
      nomeTurma  : state.sessaoReducer.turmaDisciplina.nome
  }
};


const HomeConnect = connect(mapStateToProps)(Home);

export default HomeConnect;
