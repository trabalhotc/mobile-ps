import { 
    StyleSheet,
    Dimensions, } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    bgImage: {
        flex: 1,
        top: 0,
        left: 0,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer:{
        //backgroundColor: 'trnsparent',
        width: SCREEN_WIDTH - 30,
        borderRadius: 10,
        paddingTop: 32,
        paddingBottom: 32,
        alignItems: 'center',
        
    },
    logoAnimago :{
        height : 175,
        width : 150,
        marginBottom: 20
        /*  
        animation: 'App-logo-spin infinite 20s linear'
        height: '40vmin',
        pointer-events: none;
        */
    },
    textAguardando:{
        //flex: 1,
        //height: 100,
        
        textAlign: 'center',
        fontSize : 35,
        //fontWeight : 'bold',
        //color: '#203349',
        color: '#FDFDFD',
        fontFamily: 'showcard-gothic',
    }
});

export default styles;