import React, { Component } from 'react';

import { 
  View , 
  Button,
  Text, 
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Animated,
  Easing,
  ActivityIndicator,
  ImageBackground} from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import {Icon} from 'react-native-elements'
import NotifService from '../../service/NotifService';
  


const BG_IMAGE = require('../../images/bkg.png');
const LOGO_IMAGE = require('../../images/logo_simples.png');




export class Home extends Component {

  constructor(props){
    super(props);
    this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this));
  }
  state ={
    cont : 0,
    rotateAnim: new Animated.Value(0)
  }

  onRegister(token) {
    //Alert.alert("Registered !", JSON.stringify(token));
    //console.log(token);
    //this.setState({ registerToken: token.token, gcmRegistered: true });
  }

  onNotif(notif) {
    //console.log(notif);
    //Alert.alert(notif.title, notif.message);
  }

  startAnimation () {
    this.state.rotateAnim.setValue(0)
    Animated.timing(
      this.state.rotateAnim,
      {
        toValue: 1,
        duration: 12000,
        easing: Easing.linear
      }
    ).start(() => {
      this.startAnimation()
    })
  }

  static navigationOptions = ( {navigation} ) =>({
    
    title: navigation.state.params ? navigation.state.params.myTitle : 'Titulo',//navigation.state.params.myTitle ? navigation.state.params.myTitle : ''
    
    headerStyle: {
      backgroundColor: '#203349',
      
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color:'#FDFDFD'
    },
    headerRight : (
      <TouchableOpacity style={{marginRight:8}} onPress={() =>{
        AsyncStorage.clear();
        navigation.navigate('Login');
       }} >
          <Icon
            name='exit-to-app'
            color='#FDFDFD'
            >

          </Icon>
      </TouchableOpacity>
        
     
    ),
    
  });

  

  componentDidMount = async ()=>{
    //Alert.alert(this.props.aluno.CPF);
    this.props.navigation.setParams({
      myTitle: await  AsyncStorage.getItem('@PS-NOMETURMA')
     
     });
     
     this.subscribetoEvents();
     this.startAnimation();
     
     

     
  };

  
  

  render() {
    return(
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          <Animated.Image
              source={LOGO_IMAGE}
              style={[
                styles.logoAnimago,
                {transform: [
                  {rotate: this.state.rotateAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                      '0deg', '360deg'
                    ]
                  })}
                ]} ]}
            />
          <View style={styles.formContainer}>
            
            <Text style={styles.textAguardando} onPress={()=>{this.notif.localNotif()}}>
              Aguardando Perguntas
              
            </Text>
            
            
          </View>
          
        </ImageBackground>
      </View>
    )
  }
};

const mapStateToProps = (state) =>{
  return{ 
      nomeAluno  : state.sessaoReducer.aluno.nome,
      aluno      : state.sessaoReducer.aluno,
      nomeTurma  : state.sessaoReducer.turmaDisciplina.nome
  }
};


const HomeConnect = connect(mapStateToProps)(Home);

export default HomeConnect;
