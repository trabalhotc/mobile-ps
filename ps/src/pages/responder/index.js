import React, { Component } from 'react';

import { 
  View , 
  Text, 
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Animated,
  Easing,
  ActivityIndicator,
  ImageBackground} from 'react-native';
import {Button, Icon } from 'react-native-elements';
import styles from './styles';
import { connect } from 'react-redux';
import ImageLoader from '../../components/imageLoader';
import  socket from 'socket.io-client';
import {getActiveRouteState} from '../../NavigationService'
import Constants from '../../constants'
import {io} from '../../service/api'
  

// import { Container } from './styles';


const BG_IMAGE = require('../../images/bkg.png');
const LOGO_IMAGE = require('../../images/logo_simples.png');




export class Responder extends Component {

  state ={
    cont : 0,
    rotateAnim: new Animated.Value(0),
    disabledButtonResp : false,
    showQuemResponde : false,
    nomeAlunoClicou : ''
  }

 

  startAnimation () {
    this.state.rotateAnim.setValue(0)
    Animated.timing(
      this.state.rotateAnim,
      {
        toValue: 1,
        duration: 12000,
        easing: Easing.linear
      }
    ).start(() => {
      this.startAnimation()
    })
  }

  hadleRespondere = () =>{
    //const io = socket(Constants.BASE_URL_SERVER);

    const data = {
      aluno: this.props.aluno,
      idSessao : this.props.idSessao, 
    }

    io()
      .then(instace =>{
        instace.emit('responder',this.props.aluno);
      })
    //io.emit('responder', this.props.aluno);
  }
  static navigationOptions = ( {navigation} ) =>({
    
    //title: navigation.state.params ? navigation.state.params.myTitle : 'Titulo',//navigation.state.params.myTitle ? navigation.state.params.myTitle : ''
    
    headerStyle: {
      backgroundColor: '#203349',
      tintColor : '#FDFDFD'
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color:'#FDFDFD'
    },
    headerTintColor:'#FDFDFD'

    
  });

  
  subscribetoEvents = ()=>{
    //const io = socket(Constants.BASE_URL_SERVER);

    io()
      .then(instace =>{        

        instace.on('logou', data=>{
          //Alert.alert('Professor Logou '+this.props.activeItemKey);
          //Alert.alert(getActiveRouteState(this.props.navigation.state));
        });
        instace.on('aposAcaoProfessorPApp',data=>{
          this.props.navigation.navigate('Home');
        })
        instace.on('queroResponder',data =>{
          
          
          this.setState({disabledButtonResp:true, showQuemResponde: true});
    
          if (data.id === this.props.aluno.id){
    
            this.setState({nomeAlunoClicou:'VOCê'});
          }else{
            this.setState({nomeAlunoClicou:data.nome});
          }
          
        });
    
        instace.on('perguntaApresentada', data=>{
          this.setState({disabledButtonResp:false, showQuemResponde: false});
          this.forceUpdate();
          //this.props.navigation.navigate('Responder');
        });
      })

    
  }

  componentDidMount = async ()=>{
    
    
    this.startAnimation();
    this.subscribetoEvents()
    this.setState({disabledButtonResp:false, showQuemResponde: false});
  }
  

  render() {
    const {showQuemResponde} = this.state;
    return(
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          <Text style={[styles.textNovaPergunta,
              showQuemResponde && styles.textNovaPerguntaDisbled ]} 
              disabled={this.setState.disabledButtonResp}>
              Nova Pergunta Apresentada!
          </Text>
          {this.state.showQuemResponde ? (
            <View style={styles.containerQuemResponde}>
              <Text style={styles.textQuemResponde}>
                {this.state.nomeAlunoClicou}                 
              </Text>
              <Text style={[styles.textQuemResponde, {fontSize:28}]}>
                  vai responder!
                </Text>
            </View>
            
          ):(<React.Fragment/>)}
          
          
          <Animated.Image
              
              source={LOGO_IMAGE}
              style={[
                styles.logoAnimago,
                /*{transform: [
                  {rotate: this.state.rotateAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                      '0deg', '360deg'
                    ]
                  })}
                ]} */
                ]}
            />
          <View style={styles.formContainer}>
            
            <Button
                  buttonStyle={styles.responderButton}
                  containerStyle={{ marginTop: 32, flex: 0 }}
                  //activeOpacity={0.8}
                  title={'Quero Responder!'}
                  onPress={()=>{this.hadleRespondere()}}
                  titleStyle={styles.loginTextButton}    
                  disabled ={this.state.disabledButtonResp}              
                />
            
          </View>
          
        </ImageBackground>
      </View>
    )
  }
};

const mapStateToProps = (state) =>{
  return{ 
      aluno      : state.sessaoReducer.aluno,
      idSessao   : state.sessaoReducer.idSessao,
  }
};


const ResponderConnect = connect(mapStateToProps)(Responder);

export default ResponderConnect;
