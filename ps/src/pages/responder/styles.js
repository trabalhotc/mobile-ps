import { 
    StyleSheet,
    Dimensions, } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    bgImage: {
        flex: 1,
        top: 0,
        left: 0,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer:{
    
        alignItems: 'center',
        
    },
    textQuemResponde:{
        fontSize: 42,
        color: '#FDFDFD',        
        fontFamily : 'showcard-gothic',
        textAlign : 'center',
    },
    textNovaPerguntaDisbled: {
        color: '#99A1A8', 
      },
    textNovaPergunta:{
        fontSize: 42,
        color: '#FDFDFD',        
        fontFamily : 'showcard-gothic',
        textAlign : 'center',
        //marginBottom : 25
    },
    containerQuemResponde:{
        backgroundColor: '#6193C5',
        width: SCREEN_WIDTH - 30,
        borderRadius: 10,
        paddingTop: 15,
        alignItems: 'center',               
        
    },
    loginTextButton: {
        fontSize: 30,
        color: '#FDFDFD',
        
        fontFamily : 'showcard-gothic'
      },
    responderButton: {
    backgroundColor: '#6193C5',
    borderRadius: 10,
    height: 100,
    width: SCREEN_WIDTH - 30,
    },
    logoAnimago :{
        height : 175,
        width : 150,
        marginBottom: 20,
        opacity : 0
        /*  
        animation: 'App-logo-spin infinite 20s linear'
        height: '40vmin',
        pointer-events: none;
        */
    },
    textAguardando:{
        //flex: 1,
        //height: 100,
        
        textAlign: 'center',
        fontSize : 35,
        fontWeight : 'bold',
        //color: '#203349',
        color: '#FDFDFD',
        fontFamily: 'showcard-gothic',
    }
});

export default styles;