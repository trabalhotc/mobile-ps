import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  LayoutAnimation,
  UIManager,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  StatusBar,
  Alert,
  AppState,
  Platform,
  Modal,
  Animated
} from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import ImageLoader from '../../components/imageLoader';
import {io} from '../../service/api';

//import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import { connect } from 'react-redux';
import { insertAluno } from '../../actions/alunoActions'
import { loginAuluno ,  updateConnectionServer } from '../../actions/sessaoActions';
import {constants} from '../../constants';

import AsyncStorage from '@react-native-community/async-storage';
import BackgroundTimer from 'react-native-background-timer';




//const BG_IMAGE = require('../../images/bg_screen4.jpg');
const LOGO_IMAGE = require('../../images/logo.png');
const BG_IMAGE = require('../../images/bkg_limpo.png');


// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected2} />
    </View>
  );
};

TabSelector.propTypes = {
  selected: PropTypes.bool.isRequired,
};

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      CPF: '',
      password: '',
      name : '',
      passwordConfirmation:'',
      selectedCategory: 0,
      isLoading: false,
      isCPFValid: true,
      isPasswordValid: true,
      isConfirmationValid: true,
      isNameValid: true,
      modalVisible : false,
      isIpServerValid : true,
      ipServer : "",//"192.168.0.102",

      IPInput : null,

      rotateAnim: new Animated.Value(0)
    };

    this.selectCategory = this.selectCategory.bind(this);
    this.login = this.login.bind(this);
    this.signUp = this.signUp.bind(this);
  }
  componentDidMount = async()=>{
    //AsyncStorage.clear();
    AsyncStorage.setItem('@PS-ALUNOLOGADO', ""); 
    AsyncStorage.setItem('@PS-IDSESSAO',"");
    AsyncStorage.setItem('@PS-NOMETURMA',"");
    this.startAnimation();
    //AsyncStorage.setItem('@PS-IPSERVIDOR', "192.168.0.102");
    
    
  }

  handleCloseSession =()=>{
    AsyncStorage.setItem('@PS-ALUNOLOGADO', ""); 
    AsyncStorage.setItem('@PS-IDSESSAO',"");
    AsyncStorage.setItem('@PS-NOMETURMA',"");

    this.props.navigation.navigate('Login');
  }
  selectCategory(selectedCategory) {
    LayoutAnimation.easeInEaseOut();
    this.setState({
      selectedCategory, 
      isLoading: false,
    });
    this.clearInputs();
    this.CPFInput.focus();
  }

  validateCPF(CPF) {
    var re = /^(([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2}))$/;

    return re.test(CPF);
  };

  validateIP(IP) {
    let re = /^(?:(?:^|\.)(?:2(?:5[0-5]|[0-4]\d)|1?\d?\d)){4}$/;

    return re.test(IP);
  };

  TestaCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
  if (strCPF == "00000000000") return false;
     
  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;
   
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
   
  Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
   
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
  }


  login = async () =>{

    io()
      .then(instace =>{        
        instace.on('encerrouSessao', async data=>{
          
          this.handleCloseSession();          
            
        }); 

        instace.on('disconnect', function(){
          
          instace.connect();
          console.log('Reconectou');

        }); 
        
        BackgroundTimer.runBackgroundTimer(() => { 
          console.log(' apos 3s em segundo plano') ;
          
          instace.connect();
          
        }, 
        3000);



    });
   
    this.setState({ isLoading: true });
  
    try {
      
      
      if (!this.validaAllInput()){
        
        LayoutAnimation.easeInEaseOut();
        this.setState({isLoading: false});
        //Alert.alert('titulo','invalido');
      }else{
        
        const CPF = this.state.CPF;
        const senha = this.state.password;
        //Alert.alert('cpf: '+CPF+' senha: '+senha);
        //return

        try {
           await this.props.loginAuluno({
            CPF,
            senha        
          });

          //this.selectCategory(0);
          
          LayoutAnimation.easeInEaseOut();
          this.setState({isLoading: false});
          //this.props.navigation.navigate('Home');
          
          //this.clearInputs();
        } catch (error) {
          Alert.alert('error'+error);
          LayoutAnimation.easeInEaseOut();
          this.setState({isLoading: false});
        }
      
  
      }
      
    } catch (error) {
      LayoutAnimation.easeInEaseOut();
      this.setState({isLoading: false})
    }

   
  };

  validaInputIP = () =>{
    const { ipServer } = this.state;

    var retorno = true;
    this.setState({
      isIpServerValid : this.validateIP(ipServer)
    });

    if (!this.validateIP(ipServer)){
      retorno = false;
      this.IPInput.shake();

    };

    return retorno;
  };

  validaAllInput = () => {
    
    var retorno = true;
    const { CPF, 
            name, 
            password, 
            passwordConfirmation,
            selectedCategory
          } = this.state;
    
    selectedCategory===1 ?(
      this.setState({
        isCPFValid          : this.validateCPF(CPF) && this.TestaCPF(CPF)? true : false,
        isNameValid         : name.length>=4                    ? true : false,
        isPasswordValid     : password.length >= 8              ? true : false,
        isConfirmationValid : password === passwordConfirmation ? true : false,
      }))
      : (
        this.setState({
          isCPFValid          : this.validateCPF(CPF) && this.TestaCPF(CPF)? true : false,
          isPasswordValid     : password.length >= 8              ? true : false,
        })
      )
    

    if (!this.validateCPF(CPF) || !this.TestaCPF(CPF)){
      retorno = false;
      this.CPFInput.shake();

    };
         
    if(!(name.length>=4) && selectedCategory===1){
      retorno = false;
      this.nameInput.shake();   
    }
      
    if (!(password.length >= 8) ){
      retorno = false;
      this.passwordInput.shake();
      
    }

      
          
    if (!(password === passwordConfirmation) && selectedCategory===1){
      retorno = false;
      this.confirmationInput.shake();
      
    }
      
    return retorno;
    
    
  }

  clearInputs = () =>{
    this.setState({
      CPF                  : '',
      name                 : '',
      password             : '' ,
      passwordConfirmation : '' 
    })
  }
  signUp = async  ()=>  {
    const { CPF, name, password, passwordConfirmation } = this.state;
    
    this.setState({ isLoading: true });

    
    
    try {
      
      
      if (!this.validaAllInput()){
        
        LayoutAnimation.easeInEaseOut();
        this.setState({isLoading: false});
        //Alert.alert('titulo','invalido');
      }else{
        
        const nome = this.state.name;
        const CPF = this.state.CPF;
        const senha = this.state.password;
        //console.log('valido');
        try {
           await this.props.insertAluno({
            nome,
            CPF,
            senha        
          });
          this.selectCategory(0);
        } catch (error) {

        }
        
  

  
        LayoutAnimation.easeInEaseOut();
        this.setState({isLoading: false});
        this.clearInputs();
  
      }
      
    } catch (error) {
      LayoutAnimation.easeInEaseOut();
      this.setState({isLoading: false})
    }
  }

  setModalVisible = async (value) =>{
    this.setState({modalVisible : value})
    if (this.state.ipServer == ""){
      this.setState({ipServer: await AsyncStorage.getItem('@PS-IPSERVIDOR')})
    }
  }

  hideModal = () =>{
    if (!this.validaInputIP()){
      return;
    }else{
      this.setModalVisible(false);
      
      
      AsyncStorage.setItem('@PS-IPSERVIDOR', this.state.ipServer);

     // this.props.updateConnectionServer();
      //armazena no local storage
    }
  }
  
  showModal = ()=>{
    this.setModalVisible(true);
  }

  render() {
    const {
      selectedCategory,
      isLoading,
      isCPFValid,
      isPasswordValid,
      isConfirmationValid,
      isNameValid,
      CPF,
      password,
      name,
      passwordConfirmation,
      isIpServerValid
    } = this.state;
    const isLoginPage = selectedCategory === 0;
    const isSignUpPage = selectedCategory === 1;
    let IPInput = null;
  
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#1D2E41"/>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
         
          onShow = { () => this.IPInput.focus()}
          >
          <View >
            <View style={styles.containerModal}>
              <Text style={styles.textModal}>Informe o IP do Servidor</Text>
              <Input
                    
                    value={this.state.ipServer}
                    ref={input => (this.IPInput = input)}
                    keyboardAppearance="light"
                    autoCapitalize="sentences"
                    autoCorrect={false}
                    keyboardType="number-pad"
                    returnKeyType={'done'}
                    blurOnSubmit={true} 
                    containerStyle={{
                      marginTop: 6,
                      borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                    }}
                    
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={'Exemplo 192.168.0.1'}
                    
                    
                    onChangeText={ipServer =>
                      this.setState({ ipServer })
                    }
                    errorMessage={
                      isIpServerValid
                        ? null
                        : 'Por favor, digite um IP válido! '
                    }
                  />
              <Button
                  buttonStyle={styles.loginButton}
                  containerStyle={{ marginTop: 10, flex: 0 }}
                  activeOpacity={0.8}
                  title="OK"
                  onPress={() => this.hideModal()}
                  titleStyle={styles.loginTextButton}
                                  
              />

              
            </View>
          </View>
        </Modal>

        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          <View>
          
            <KeyboardAvoidingView
              contentContainerStyle={styles.loginContainer}
              behavior={Platform.OS ==='ios' ? 'padding' : ''} 
            >
            
              <View style={styles.titleContainer}>
                <ImageLoader
                  source={LOGO_IMAGE}
                  style={[
                    styles.loginLogo                
                  ]} 
                />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Button
                  disabled={isLoading}
                  type="clear"
                  activeOpacity={0.7}
                  onPress={() => this.selectCategory(0)}
                  containerStyle={{ flex: 1 }}
                  titleStyle={[
                    styles.categoryText,
                    isLoginPage && styles.selectedCategoryText,
                  ]}
                  title={'Entrar'}
                />
                <Button
                  disabled={isLoading}
                  type="clear"
                  activeOpacity={0.7}
                  onPress={() => this.selectCategory(1)}
                  containerStyle={{ flex: 1 }}
                  titleStyle={[
                    styles.categoryText,
                    isSignUpPage && styles.selectedCategoryText,
                  ]}
                  title={'Cadastrar-se'}
                />
              </View>
              <View style={styles.rowSelector}>
                <TabSelector selected={isLoginPage} />
                <TabSelector selected={isSignUpPage} />
              </View>
              
              <View style={styles.formContainer}>
                <Input
                  
                  value={CPF}
                  keyboardAppearance="light"
                  autoFocus={false}
                  
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="number-pad"
                  returnKeyType="next"
                  inputStyle={{ marginLeft: 10 }}
                  placeholder={'CPF'}
                  maxLength={11}
                  containerStyle={{
                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                  }}
                  ref={input => (this.CPFInput = input)}
                  onSubmitEditing={() => this.nameInput.focus()}
                  onChangeText={CPF => this.setState({ CPF })}
                  errorMessage={
                    isCPFValid ? null : 'Por favor, digite um CPF válido'
                  }
                />
                {isSignUpPage && (
                  <Input
                    
                    value={name}
                    
                    keyboardAppearance="light"
                    autoCapitalize="sentences"
                    autoCorrect={false}
                    keyboardType="default"
                    returnKeyType={'next'}
                    blurOnSubmit={true}
                    containerStyle={{
                      marginTop: 16,
                      borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                    }}
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={'Nome'}
                    ref={input => (this.nameInput = input)}
                    onSubmitEditing={()=> this.passwordInput.focus()}
                    onChangeText={name =>
                      this.setState({ name })
                    }
                    errorMessage={
                      isNameValid
                        ? null
                        : 'Por favor, digite seu nome. Mínomo caracteres '
                    }
                  />
                )}
                
                
                <Input
                  
                  value={password}
                  keyboardAppearance="light"
                  autoCapitalize="none"
                  autoCorrect={false}
                  secureTextEntry={true}
                  returnKeyType={isSignUpPage ? 'next' : 'done'}
                  blurOnSubmit={true}
                  containerStyle={{
                    marginTop: 16,
                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                  }}
                  inputStyle={{ marginLeft: 10 }}
                  placeholder={'Senha'}
                  maxLength={8}
                  ref={input => (this.passwordInput = input)}
                  onSubmitEditing={() =>
                    isSignUpPage ? this.confirmationInput.focus() : this.login()
                  }
                  onChangeText={password => this.setState({ password })}
                  errorMessage={
                    isPasswordValid
                      ? null
                      : 'Por favor, digite uma senha de 8 caracteres'
                  }
                />
                {isSignUpPage && (
                  <Input
                    
                    value={passwordConfirmation}
                    secureTextEntry={true}
                    keyboardAppearance="light"
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    returnKeyType={'done'}
                    blurOnSubmit={true}
                    containerStyle={{
                      marginTop: 16,
                      borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                    }}
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={'Confirme a senha'}
                    maxLength={8}
                    ref={input => (this.confirmationInput = input)}
                    onSubmitEditing={this.signUp}
                    onChangeText={passwordConfirmation =>
                      this.setState({ passwordConfirmation })
                    }
                    errorMessage={
                      isConfirmationValid
                        ? null
                        : 'Por favor, digite a mesma senha'
                    }
                  /> 
                )}
                <Button
                  buttonStyle={styles.loginButton}
                  containerStyle={{ marginTop: 32, flex: 0 }}
                  activeOpacity={0.8}
                  title={isLoginPage ? 'ENTRAR' : 'CADASTRAR'}
                  onPress={isLoginPage ? this.login : this.signUp}
                  titleStyle={styles.loginTextButton}
                  loading={isLoading}
                  
                  disabled={isLoading}
                />
              </View>
              
              
            </KeyboardAvoidingView>
            
            
          </View>
          
          <Text style={styles.textConfig}
            onPress={this.showModal}>
            Configurar IP do servidor
         </Text>
        </ImageBackground>
        
      </View>
    );
  }
};


const mapStateToProps = (state) =>{
  return{ 
      id :   state.alunoReducer.id,
      CPF :   state.alunoReducer.cpf,
      name :   state.alunoReducer.nome,
      password :   state.alunoReducer.senha,
  }
};


const LoginConnect = connect(mapStateToProps,{ insertAluno , loginAuluno, updateConnectionServer})(Login);

export default LoginConnect;



