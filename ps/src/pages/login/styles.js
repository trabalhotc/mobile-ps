
import { 
    StyleSheet,
    Dimensions, } from 'react-native';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    rowSelector: {
      height: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    selectorContainer: {
      flex: 1,
      alignItems: 'center',
    },
    selected: {
      position: 'absolute',
      borderRadius: 50,
      height: 0,
      width: 0,
      top: -5,
      borderRightWidth: 70,
      borderBottomWidth: 70,
      borderColor: 'white',
      backgroundColor: 'white',
    },
    selected2 :{
      //top: 0,
      //top :10,
      width: 45,
      height: 45, 
      marginBottom : -30,
      backgroundColor: 'white', 
      //transform :   [{ rotateX: '45deg' }, { rotateZ: '45deg' },{ rotateY: '45deg' }],
      transform :   [{ rotateZ: '45deg' }]
    },
    loginContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    loginTextButton: {
      fontSize: 16,
      color: 'white',
      fontWeight: 'bold',
    },
    loginButton: {
      //backgroundColor: 'rgba(232, 147, 142, 1)',
      backgroundColor: '#6193C5',
      borderRadius: 10,
      height: 50,
      width: 200,
    },
    textConfig: {
      marginTop : 5,
      color: '#D3D3D3',
      fontSize: 13
      
    },
    textModal: {
      color: '#6B6666',
      fontSize: 25,
      fontFamily: 'regular',
    },
    containerModal :{
      marginTop: "10%",
      alignItems: 'center',
      //justifyContent: 'center',
      flexDirection : 'column',
      
    },
    loginLogo:{
        //alignSelf : 'center'
    },
    titleContainer: { 
      height: 100,
      backgroundColor: 'transparent',
      justifyContent: 'flex-end',
      alignItems :'center',
      flexDirection :'column'
    },
    formContainer: {
      backgroundColor: 'white',
      width: SCREEN_WIDTH - 30,
      borderRadius: 10,
      paddingTop: 32,
      paddingBottom: 32,
      alignItems: 'center',
    },
    loginText: {
      fontSize: 16,
      fontWeight: 'bold',
      color: 'white',
    
    },
    bgImage: {
      flex: 1,
      top: 0,
      left: 0,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      justifyContent: 'center',
      alignItems: 'center',
    },
    categoryText: {
      textAlign: 'center',
      color: 'white',
      fontSize: 24,
      fontFamily: 'ligth',
      backgroundColor: 'transparent',
      opacity: 0.54,

    },
    selectedCategoryText: {
      opacity: 1,
      
    },
    titleText: {
      color: 'white',
      fontSize: 30,
      fontFamily: 'regular',
    },
    helpContainer: {
      height: 64,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

  export default styles;