import React from 'react';

import { AppState } from 'react-native';
import Routes from './routes';
import {ThemeProvider} from 'react-native-elements';
import { Provider } from 'react-redux';

import { store } from './store'; 
import NavigationService from './NavigationService';

//import firebase from 'react-native-firebase';
//import AsyncStorage from '@react-native-community/async-storage';

const App = ( ) => {
    
    /* firebase
    getToken = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log("before fcmToken: ", fcmToken);
        if (!fcmToken) {
          fcmToken = await firebase.messaging().getToken();
          if (fcmToken) {
            console.log("after fcmToken: ", fcmToken);
            await AsyncStorage.setItem('fcmToken', fcmToken);
          }
        }
      } 
      
      checkPermission = async () => {
        
        firebase.messaging().hasPermission()
          .then(enabled => {
            if (enabled) {
              console.log("Permission granted");
              this.getToken();
            } else {
              console.log("Request Permission");
              this.requestPermission(); 
            } 
          });
      }

      */
      componentDidMount = async ( ) =>{
    
        //this.checkPermission();
      }
       
    
    //this.notif = new NotifService(onRegister.bind(this), onNotif.bind(this));
    
    //this.checkPermission();  

    console.disableYellowBox = true;
    return( 
        <Provider store={store} >
            <ThemeProvider >
                
                <Routes  ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}/>
                
            </ThemeProvider>
        </Provider>
           
    )
    
    
};


export default App;
