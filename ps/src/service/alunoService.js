import api from './api'

 const AlunoService = {


    save : function (data){

        return new Promise(function(resolve,reject) {
            
            api()
                .then(instance =>{
                    const resp = instance.post('aluno/',data);
                    if (resp!=null){
                
                
                        return resolve(resp);
                    }else{
                        console.log('erro login');
                        return reject('Dados não foram salvos!');
                    } 
                })

              
        
        })
    },
    login : function  (data){
        
        return new Promise(function(resolve,reject) {
            
            
            api()
                .then(instance =>{
                    const resp = instance.post('alunoAuth/',data);
                    if (resp!=null){
                
                
                        return resolve(resp);
                    }else{
                        console.log('erro login');
                        return reject('Dados não foram verificados!');
                    } 
                })//post('alunoAuth/',data);
               
        
        })
    },
};


export default AlunoService;